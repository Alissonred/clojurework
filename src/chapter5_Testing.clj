(ns chapter5_Testing)

(use 'clojure.test)
;INCISO 1/////////////////////////////////////////////////////////////////////////////////////////////////////
(defn attr
  "Función que retorna el valor del atributo hijo de :attributes  "
  [key entidad]
  (get-in entidad [:attributes key])
  )
(deftest test-attr
  (def person {:name "fulanito" :attributes {:intelligence 120 :height "1,75 m"}})
  (is (= 120 (attr :intelligence person)))
  (is (= "1,75 m" (attr :height person)))
  )
;INCISO 2/////////////////////////////////////////////////////////////////////////////////////////////////////
(defn my-comp
  "Implementación de la función comp"
  [func1 func2 value]
  (func1(func2 value))
  )
(deftest test-my-comp
  (def person {:name "fulanito" :attributes {:intelligence 120 :height "1,75 m"}})
  (is (= 120 (my-comp :intelligence :attributes person)))
  (is (= "1,75 m" (my-comp :height :attributes person)))
  )
;INCISO 3/////////////////////////////////////////////////////////////////////////////////////////////////////

(defn my-assoc-in
  "Implementación de la función assoc-in"
  [mapy [key1 & others-key] value]
  (if (empty? others-key)
    (assoc mapy key1 value )
    (assoc mapy key1 (my-assoc-in {} others-key value ))
    )
  )
(deftest test_my-assoc-in
  (def nested_attribute1 {:clave1 {:clave2 {:clave3 "valor"}}} )
  (def nested_attribute2 {:clave1 {:clave2 "valor"}} )
  (is (= nested_attribute1 (my-assoc-in {} [:clave1 :clave2 :clave3] "valor")))
  (is (= nested_attribute2 (my-assoc-in {} [:clave1 :clave2] "valor")))
  )

;INCISO 4/////////////////////////////////////////////////////////////////////////////////////////////////////
;na
;INCISO 5/////////////////////////////////////////////////////////////////////////////////////////////////////
(defn my-update-in
  "Implementación de la función update-in"
  [mapy [key] func new_value ]
  (into {} (map (fn [[k v]]
                  (assoc {} k (if (= key k)
                                (func v new_value )
                                v
                                ))
                  ) mapy))
  )
(deftest test-my-update-in
  (def student {:name "Ana" :age 23 :course "math"})
  (def student_updated1 {:name "Ana", :age 26, :course "math"})
  (def student_updated2 {:name "Ana Milena", :age 23, :course "math"})
  (is (= student_updated1 (my-update-in student [:age] + 3 )))
  (is (= student_updated2 (my-update-in student [:name] str " Milena" )))
  )
;//////////////////////////////////////////////
(run-tests)


;///////////////////////////////////////////////////////////////////////////////////////////////////////////