(ns excChapter4)
(def filename "sospechosos.csv")
;FUNCIONES DE LECTURA ///////////////////////////////////////////////////////////////////////////////////////////////
(def vamp-keys [:name :glitter-index])                      ;estructura que llevará
(defn str->int
  [str]
  (Integer. str))                                           ; convierte a entero
;(println (+ 3 (str->int "2")))

(def conversions {:name          identity                   ;arma el objeto con funciones
                  :glitter-index str->int})

(defn convert
  [vamp-key value]
  ((get conversions vamp-key) value))                       ; hace la conversion de los datos a identidad o entero
;(println (convert :glitter-index "3") )


(defn parse [string]
  (map #(clojure.string/split % #",") (clojure.string/split string #"\r\n")) ) ; parsea los datos del cvs

;(println (parse (slurp filename)))

(defn mapify
  [rows]
  (map (fn [unmapped-row]
         (reduce (fn [row-map [vamp-key value]]
                   (assoc row-map vamp-key (convert vamp-key value)))
                 {}
                 (map vector vamp-keys unmapped-row)))
       rows))

;(println (mapify (parse (slurp filename))))
;INCISO 1 ///////////////////////////////////////////////////////////////////////////////////////////////
(defn glitter-filter
  "Función que retorna los nombres de la lista de sospechosos filtrada"
  [minimum-glitter records]
  (map (fn [inmate] (:name inmate)) (filter #(>= (:glitter-index %) minimum-glitter) records))
  )

;(println (glitter-filter 3 (mapify (parse (slurp filename)))))

;INCISO 2////////////////////////////////////////////////////////////////////////////////////////////////////
(defn append
  "Función que permite agregar sospechosos a la lista inicial"
  [name glitter list-suspect]
  (conj list-suspect (vector name (Integer. glitter) ))
  )
;(println (append "Ane Pottman" 3 (parse (slurp filename))))
(defn append-mapped
  "Función que permite agregar sospechosos a la lista ya mapeada"
  [name glitter list-suspect]
  (conj list-suspect (hash-map :name name  :glitter-index (Integer. glitter) ))
  )
;(println (append-mapped "Ane Pottman" "3" (mapify (parse (slurp filename)))))

;INCISO 3///////////////////////////////////////////////////////////////////////////////////////////////////////
(def conv {:name (fn [indiv](string? (:name indiv))) :glitter-index (fn [indiv](int? (:glitter-index indiv)))})
(defn validate
  "Función que comprueba que name y glitter-index están presentes al anexar un sospechoso "
  [evaluator suspect-maped]
  (if (and ((get evaluator :name)suspect-maped) ((get evaluator :glitter-index)suspect-maped))    ;ejecución de funciones guardadas en conv
    true
    false
    )
  )
;(println (validate conv {:name "Carlisle Cullen", :glitter-index 6}))

;INCISO 4 ///////////////////////////////////////////////////////////////////////////////////////////
(defn convert-to-csv
  "Función que retorna los sospechosos en formato CSV a partir de una lista mapeada y parseada"
  [list-mapped]
  (clojure.string/join "\n" (map (fn [suspect] (str (:name suspect) "," (:glitter-index suspect)) ) list-mapped
                                 ))
  )
;(println (convert-to-csv (mapify (parse (slurp filename)))))

;(println ((fn [suspect] (:name suspect) ){:name "Carlisle Cullen", :glitter-index 6}))
