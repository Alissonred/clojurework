(ns excChapter3)

(defn saluda [nombre]
  (println (str "hola, " nombre)))
(saluda "alisson")

(def family (vector "eliana" "maria" "pedro" "emanuel" "isabel" "vicente"))
(def task (list "wash dishes" "clean table" "wash shoes"))
(def aboutme (hash-map :name "alisson" :age 23 :career "developer"))
(def importantStars (hash-set "cirio" "betelgeuse" "alfa centauri" "canopus" "cirio" "betelgeuse"))
;(println importantStars aboutme task family)

;Inciso2//////////////////////////////////////////////////////////////////////////////////////////////////////
(defn increase [number]
  "Función que incrementa el numero ingresado en 100"
  (+ number 100)
  )

;Inciso 3//////////////////////////////////////////////////////////////////////////////////////////////////
(defn dec-maker [step]
  "Función que define pasos a disminuir un numero dado "
  #(- % step))
(def stepBy9 (dec-maker 9))
;(println (stepBy9 5))

;Inciso 4 /////////////////////////////////////////////////////////////////////////////////////////////////////
(defn mapset
  "Retorna el equivalente a set de la colección ingresada"
  [func input]
  (set (map func input)))
;(println(mapset inc [1 1 2 2]))

; Inciso 5//////////////////////////////////////////////////////////////////////////////////////////////

(def asym-parts [{:name "head" }
                 {:name "left-eye" }
                 {:name "left-ear" }
                 {:name "mouth" }
                 {:name "nose" }
                 {:name "neck" }
                 {:name "left-shoulder" }
                 {:name "left-upper-arm"}
                 {:name "chest" }
                 {:name "back"}
                 {:name "left-forearm" }
                 {:name "abdomen" }
                 {:name "left-kidney" }
                 {:name "left-hand" }
                 {:name "left-knee" }
                 {:name "left-thigh" }
                 {:name "left-lower-leg" }
                 {:name "left-achilles" }
            {:name "left-foot" }])

(defn radial
  "Función que recibe una parte del cuerpo y retorna la parte simetrizada en 5  "
      [part]
      (if (clojure.string/includes? (:name part) "left-" );si incluye left
        (map (fn [[ind1 ind2]] {ind1 ind2})(reduce (fn [aditional-parts one-part]
                    (into aditional-parts {:name (clojure.string/replace (:name part) #"^left-" (str "part-" one-part "-"))} ))
                []
                [1 2 3 4 5]
                ))
        (map (fn [[ind1 ind2]] {ind1 ind2}) part)
        )
      )
;(println (radial {:name "Head" }))

(defn radial-symmetrize
  "Función que recibe array de partes del cuerpo y los retorna simetrizados según corresponda"
      [parts]
  (reduce (fn [final-parts part]
            (into final-parts (radial part) ))
          []
          parts))
;(println (radial-symmetrize asym-parts) )

; Inciso 6///////////////////////////////////////////////////////////////////////////////////////////////

(defn numbers
  "Función que retorna una secuencia de numeros desde 1, función auxiliar de radial-count "
      [n]
   (cons n (lazy-seq( numbers (+ n 1))))
  )
;(println (take 10 (numbers 1) ))
(defn radial-count
  "Función que recibe una parte del cuerpo y retorna la parte simetrizada  count veces  "
      [part count]
      (if (clojure.string/includes? (:name part) "left-" )
        (map (fn [[ind1 ind2]] {ind1 ind2})(reduce (fn [aditional-parts one-part]
                    (into aditional-parts {:name (clojure.string/replace (:name part) #"^left-" (str "part-" one-part "-"))} ))
                []
                (take count (numbers 1) )
                ))
        (map (fn [[ind1 ind2]] {ind1 ind2}) part)
        ))
;(println (radial-count {:name "left-eye" } 3))

(defn radial-symmetrize-count
  "Función que recibe array de partes del cuerpo y los retorna simetrizados count veces según corresponda"
      [parts count]
  (reduce (fn [final-parts part]
            (into final-parts (radial-count part count) ))
          []
          parts
          ))
;(println (radial-symmetrize-count asym-parts 4) )

;/////////////////////////////////////////

