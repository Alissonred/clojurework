(ns chapter3_Testing)
(use 'clojure.test)
;INCISO 2///////////////////////////////////////////////////////////////////////////////////////////////////////////////
(defn increase [number]
  "Función que incrementa el numero ingresado en 100"
  (+ number 100)
  )
(deftest test-increase
  (is (= 104 (increase 4)))
  (is (= 100 (increase 0))))

;INCISO 3//////////////////////////////////////////////////////////////////
(defn dec-maker [step]
  "Función que define pasos a disminuir un numero dado "
  #(- % step))
;(def stepBy9 (dec-maker 9))
(deftest test-dec-maker
  (is (= 1 ((dec-maker 9)10)))
  (is (= -4 ((dec-maker 9)5))))

;INCISO 4//////////////////////////////////////////////////////////////////

(defn mapset
  "Retorna el equivalente a set de la colección ingresada"
  [func input]
  (set (map func input)))
(deftest test-maps
  (is (= #{2 3} (mapset inc [1 1 2 2])))
  (is (= #{3 4} (mapset inc [2 2 3 3])))
  )
;INCISO 5//////////////////////////////////////////////////////////////////
(defn radial
  "Función que recibe una parte del cuerpo y retorna la parte simetrizada en 5  "
  [part]
  (if (clojure.string/includes? (:name part) "left-" );si incluye left
    (map (fn [[ind1 ind2]] {ind1 ind2})(reduce (fn [aditional-parts one-part]
                                                 (into aditional-parts {:name (clojure.string/replace (:name part) #"^left-" (str "part-" one-part "-"))} ))
                                               []
                                               [1 2 3 4 5]))
    (map (fn [[ind1 ind2]] {ind1 ind2}) part)
    ))

(deftest test-radial
  (def simetric-part '({:name "part-1-eye"} {:name "part-2-eye"} {:name "part-3-eye"} {:name "part-4-eye"} {:name "part-5-eye"}))
  (is (= simetric-part (radial {:name "left-eye" })))
  (is (= '({:name "Head"}) (radial {:name "Head" })))
  )

(defn radial-symmetrize
  "Función que recibe array de partes del cuerpo y los retorna simetrizados según corresponda"
  [parts]
  (reduce (fn [final-parts part]
            (into final-parts (radial part) ))
          []
          parts))

(deftest test-radial-symmetrize
  (def asym-parts [{:name "head" } {:name "left-eye" } {:name "left-ear" } {:name "mouth" }])
  (def sym-parts [{:name "head" } {:name "part-1-eye" } {:name "part-2-eye" } {:name "part-3-eye" } {:name "part-4-eye" } {:name "part-5-eye" } {:name "part-1-ear" } {:name "part-2-ear" } {:name "part-3-ear" } {:name "part-4-ear" } {:name "part-5-ear" } {:name "mouth" }])
  (is (= sym-parts (radial-symmetrize asym-parts)))
  )
;INCISO 6 /////////////////////////////////////////////////////////////////////////////////////////////
(defn numbers
  "Función que retorna una secuencia de numeros desde 1, función auxiliar de radial-count "
  [n] (cons n (lazy-seq( numbers (+ n 1))))
  )
(deftest test-numbers
  (is (= '(1 2 3 4 5) (take 5 (numbers 1))))
  (is (= '() (take 0 (numbers 11))))
  )
(defn radial-count
  "Función que recibe una parte del cuerpo y retorna la parte simetrizada  count veces  "
  [part count]
  (if (clojure.string/includes? (:name part) "left-" )
    (map (fn [[ind1 ind2]] {ind1 ind2})(reduce (fn [aditional-parts one-part]
                                                 (into aditional-parts {:name (clojure.string/replace (:name part) #"^left-" (str "part-" one-part "-"))} ))
                                               []
                                               (take count (numbers 1) )
                                               ))
    (map (fn [[ind1 ind2]] {ind1 ind2}) part)
    ))
(deftest test-radial-count
  (def simetric-part '({:name "part-1-eye"} {:name "part-2-eye"} {:name "part-3-eye"} ))
  (is (= simetric-part (radial-count {:name "left-eye" } 3 )))
  (is (= '({:name "Head"}) (radial-count {:name "Head" } 5 )))
  )

(defn radial-symmetrize-count
  "Función que recibe array de partes del cuerpo y los retorna simetrizados count veces según corresponda"
  [parts count]
  (reduce (fn [final-parts part]
            (into final-parts (radial-count part count) ))
          []
          parts
          ))

(deftest test-radial-symmetrize-count
  (def asym-parts [{:name "head" } {:name "left-eye" } {:name "left-ear" } {:name "mouth" }])
  (def sym-parts [{:name "head" } {:name "part-1-eye" } {:name "part-2-eye" } {:name "part-3-eye" } {:name "part-1-ear" } {:name "part-2-ear" } {:name "part-3-ear" } {:name "mouth" }])
  (is (= sym-parts (radial-symmetrize-count asym-parts 3)))
  )

;///////////////////////////////////////////////////////////////////////////////////////////////////////////////
(run-tests)

;/////////////////////////////////

