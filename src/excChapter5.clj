(ns excChapter5)

;INCISO 1/////////////////////////////////////////////////////////////////////////////////////////////////

(def pers {:name "fulanito" :attributes {:intelligence 120}})
(defn attr
  "Función que retorna el valor del atributo hijo de :attributes  "
  [key entidad]
  (get-in entidad [:attributes key])
  )
;(println ((comp :intelligence :attributes)pers))
;(println (attr :intelligence pers))

;INCISO 2/////////////////////////////////////////////////////////////////////////////////////////////////////

(defn my-comp
  "Implementación de la función comp"
  [func1 func2 value]
  (func1(func2 value))
  )
;(println (my-comp :intelligence :attributes pers))
;INCISO 3//////////////////////////////////////////////////////////////////////////////////////////////////////////

(defn my-assoc-in
  "Implementación de la función assoc-in"
  [mapy [key1 & others-key] value]
  (if (empty? others-key)
    (assoc mapy key1 value )
    (assoc mapy key1 (my-assoc-in {} others-key value ))
    )
  )
;(println (my-assoc-in {} [:clave1 :clave2 :clave3] "valor"))

;INCISO 4 //////////////////////////////////////////////////////////////////////////////////////////////////////

(def student {:name "Ana" :age 23 :course "math"})
(def current (update-in student [:name] str " milena"))
(println current)

;INCISO 5///////////////////////////////////////////////////////////////////////////////////////////////////////
(defn my-update-in
  "Implementación de la función update-in"
  [mapy [key] func new_value ]
  (into {} (map (fn [[k v]]
         (assoc {} k (if (= key k)
                       (func v new_value )
                       v
                       ))
         ) mapy))
  )
;(println (my-update-in student [:age] + 3 ) )


