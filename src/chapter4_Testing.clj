(ns chapter4_Testing)
(use 'clojure.test)
;INCISO 1/////////////////////////////////////////////////////////////////////////////////////////////////////
(defn glitter-filter
  "Función que retorna los nombres de la lista de sospechosos filtrada"
  [minimum-glitter records]
  (map (fn [inmate] (:name inmate)) (filter #(>= (:glitter-index %) minimum-glitter) records)))
(deftest test-glitter-filter
  (def mapyfy '({:name "Edward Cullen", :glitter-index 10} {:name "Bella Swan", :glitter-index 0} {:name "Charlie Swan", :glitter-index 0} {:name "Jacob Black", :glitter-index 3} {:name "Carlisle Cullen", :glitter-index 6}))
  (def names_min_3 '("Edward Cullen" "Jacob Black" "Carlisle Cullen"))
  (def names_min_0 '("Edward Cullen" "Bella Swan" "Charlie Swan" "Jacob Black" "Carlisle Cullen"))
  (is (= names_min_3 (glitter-filter 3 mapyfy)))
  (is (= names_min_0 (glitter-filter 0 mapyfy))))

;INCISO 2/////////////////////////////////////////////////////////////////////////////////////////////////////
(defn append-mapped
  "Función que permite agregar sospechosos a la lista ya mapeada"
  [name glitter list-suspect]
  (conj list-suspect (hash-map :name name  :glitter-index (Integer. glitter) ))
  )

(deftest test-append-mapped
  (def suspects '({:name "Edward Cullen", :glitter-index 10} {:name "Bella Swan", :glitter-index 0} {:name "Charlie Swan", :glitter-index 0} ))
  (def new_suspects '({:name "Ana Stevenson", :glitter-index 5} {:name "Edward Cullen", :glitter-index 10} {:name "Bella Swan", :glitter-index 0} {:name "Charlie Swan", :glitter-index 0} ))
  (def new_suspect_name "Ana Stevenson" )
  (def new_suspect_glitter 5)
  (is (= new_suspects (append-mapped new_suspect_name new_suspect_glitter suspects)))
  )

;INCISO 3/////////////////////////////////////////////////////////////////////////////////////////////////////
(defn validate
  "Función que comprueba que name y glitter-index están presentes al anexar un sospechoso "
  [evaluator suspect-maped]
  (if (and ((get evaluator :name)suspect-maped) ((get evaluator :glitter-index)suspect-maped))    ;ejecución de funciones guardadas en conv
    true
    false
    ))
(deftest test-validate
  (def conv {:name (fn [indiv](string? (:name indiv))) :glitter-index (fn [indiv](int? (:glitter-index indiv)))})
  (is (= true (validate conv {:name "Carlisle Cullen", :glitter-index 6})))
  (is (= false (validate conv {:name "Carlisle Cullen", :glitter-index "algo"})))
  (is (= false (validate conv {:name 5, :glitter-index "algo"})))
  )
;INCISO 4/////////////////////////////////////////////////////////////////////////////////////////////////////
(defn convert-to-csv
  "Función que retorna los sospechosos en formato CSV a partir de una lista mapeada y parseada"
  [list-mapped]
  (clojure.string/join "\n" (map (fn [suspect] (str (:name suspect) "," (:glitter-index suspect)) ) list-mapped)))

(deftest test_convert-to-csv
  (def mapped_suspects '({:name "Edward Cullen", :glitter-index 10} {:name "Bella Swan", :glitter-index 0} {:name "Charlie Swan", :glitter-index 0}  ))
  (def to_csv  "Edward Cullen,10\nBella Swan,0\nCharlie Swan,0" )

  (is (= to_csv (convert-to-csv mapped_suspects )))
              ;(is (= (slurp filename) (validate conv {:name "Carlisle Cullen", :glitter-index "algo"})))
  )
;//////////////////////////////////////////////
(run-tests)

;////////////////////////////////////////////////////////////////////////////////////////////////////////////
